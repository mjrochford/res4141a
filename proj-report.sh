echo "# WORKDIR ====" ; du -hsc * 2>/dev/null
echo "# LINES ====" ; wc -l *.scm
echo "# GITFILES ====" ; git ls-files | xargs du -hsc
