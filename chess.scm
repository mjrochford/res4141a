(define-module (res4141a chess)
  #:use-module (res4141a prelude)
  #:export (board-set!
            board-render-html
            make-board
            *chess-chars*
            w-king-char 
            w-queen-char
            w-rook-char 
            w-bishop-char
            w-knight-char
            w-pawn-char 
            b-king-char 
            b-queen-char
            b-rook-char 
            b-bishop-char
            b-knight-char
            b-pawn-char))

(define (board-set! b row col v)
  (vector-set! b (+ col (* 8 row)) v))
(define (board-render-html b)
  `((button "yo")
    (hr)
    (table (tr ,(for x in '(♠ A B C D E F G H ♣) `(th (flex ,x))))
           ,(for i in (reverse (range 0 7))
                 `(tr (th ,(+ 1 i))
                      ,(for j in (range 0 7)
                            (let ((space (vector-ref b (+ j (* 8 i)))))
                              `(td (@ (class ,(format #f "board-~a-~a" i j))) ,space))
                            )
                      (th ,(+ 1 i))))
           (tr ,(for x in '(♡ A B C D E F G H ♢) `(th ,x)))))
  )
(define (make-board n)
  (let ((gboard (make-vector (* n n) " ")))
    (begin
      (for x in (range 0 7)
           (begin
             (board-set! gboard 1 x w-pawn-char)
             (board-set! gboard 6 x b-pawn-char)))
      (board-set! gboard 0 0 w-rook-char)
      (board-set! gboard 0 7 w-rook-char)
      (board-set! gboard 0 1 w-knight-char)
      (board-set! gboard 0 6 w-knight-char)
      (board-set! gboard 0 2 w-bishop-char)
      (board-set! gboard 0 5 w-bishop-char)
      (board-set! gboard 0 3 w-queen-char)
      (board-set! gboard 0 4 w-king-char)

      (board-set! gboard 7 0 b-rook-char)
      (board-set! gboard 7 7 b-rook-char)
      (board-set! gboard 7 1 b-knight-char)
      (board-set! gboard 7 6 b-knight-char)
      (board-set! gboard 7 2 b-bishop-char)
      (board-set! gboard 7 5 b-bishop-char)
      (board-set! gboard 7 3 b-queen-char)
      (board-set! gboard 7 4 b-king-char)
      ) gboard))

;; CHESS CONSTANTS ----
(define cc-base 9812)
(define cc-len 11)
(define *chess-chars* (map integer->char (range cc-base (+ cc-len cc-base))))
(define w-king-char (nth *chess-chars* 0))
(define w-queen-char (nth *chess-chars* 1))
(define w-rook-char (nth *chess-chars* 2))
(define w-bishop-char (nth *chess-chars* 3))
(define w-knight-char (nth *chess-chars* 4))
(define w-pawn-char (nth *chess-chars* 5)) ;
(define b-king-char (nth *chess-chars* 6))
(define b-queen-char (nth *chess-chars* 7))
(define b-rook-char (nth *chess-chars* 8))
(define b-bishop-char (nth *chess-chars* 9))
(define b-knight-char (nth *chess-chars* 10))
(define b-pawn-char (nth *chess-chars* 11))
