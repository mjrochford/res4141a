(define-module (res4141a main)
  #:use-module (res4141a templates)
  #:use-module (res4141a chess)
  #:use-module (ice-9 ports)
  #:use-module (rnrs io ports)
  #:use-module (sxml simple)
  )

(define (render-distfiles)
  (begin
    (define *dist-dir* "dist")

    (define assets '(style.css banner.jpg))
    (map (lambda (a)
           (call-with-output-file (format #f "~a/~a" *dist-dir* a)
             (lambda (p)
               (begin
                 (cond
                  ((eq? a 'style.css) (put-string p "* { font-family: 'Iosevka', 'Iosevka Term', monospace; }
@font-face {
    font-family: 'Iosevka';
    src: url('https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/Iosevka/IosevkaNerdFont-Regular.ttf') format('truetype'); /* Chrome 4+, Firefox 3.5, Opera 10+, Safari 3—5 */
}  
html {
 box-sizing: border-box;
 margin:3rem;
}
body {
 filter: invert(100%);
 background-color: #333;
}"))
                  ((eq? a 'banner.jpg)
                   (put-bytevector p (call-with-input-file (if (eq? 0 (random 2))
                                             "Gemini_Generated_Image_e2d7c6e2d7c6e2d7.jpg"
                                             "Gemini_Generated_Image_kgnulekgnulekgnu.jpg")
                     get-bytevector-all))
                   )
                  )
                 )
               )))
         assets)
    (define routes '(index game chartable))
    (map (lambda (r)
           (call-with-output-file (format #f "~a/~a.html" *dist-dir* r)
             (lambda (p)
               (begin
                 (put-string p "<!DOCTYPE html>\n")
                 (cond
                  ((eq? r 'index) (sxml->xml index-html p))
                   ((eq? r 'game) (sxml->xml (chess-table-html (make-board 8)) p))
                  ((eq? r 'chartable) (sxml->xml chess-char-table-html p)))))
             #:encoding "UTF-8"))
         routes)
    ))
(render-distfiles)


