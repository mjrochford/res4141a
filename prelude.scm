(define-module (res4141a prelude)
  #:use-module (srfi srfi-1)
  #:use-module (sxml simple)
  #:use-module (web uri)
  #:use-module (web request)
  #:export (for
            range
            curry
            nth
            mod
            request-path-components
            sxml->string
            sxml->html-response))

;; PRELUDE ----
(define-syntax for
  (syntax-rules (in)
    ((for element in list body ...)
     (map (lambda (element) 
            body ...)
          list))))
(define (range a b)
  (cond ((> a b) '())
        (else (cons a (range (+ 1 a) b)))))
(define (curry fn . args)
  (lambda args-remaining
    (apply fn (append args args-remaining))))
(define (nth lst k) (car (drop lst k)))
(define mod floor-remainder)

;; HELPERS ----
(define (request-path-components request)
  (split-and-decode-uri-path (uri-path (request-uri request))))
(define (sxml->string tree)
  (call-with-output-string
    (lambda (p) (sxml->xml tree p))))
(define (sxml->html-response tree)
  (values '((content-type . (text/html))) (sxml->string tree)))
