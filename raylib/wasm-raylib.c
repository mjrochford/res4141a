/*******************************************************************************************
 *
 *   raylib [core] example - Basic window (adapted for HTML5 platform)
 *
 *   NOTE: This example is prepared to compile for PLATFORM_WEB, and PLATFORM_DESKTOP
 *   As you will notice, code structure is slightly diferent to the other examples...
 *   To compile it for PLATFORM_WEB just uncomment #define PLATFORM_WEB at beginning
 *
 *   Example originally created with raylib 1.3, last time updated with raylib 1.3
 *
 *   Example licensed under an unmodified zlib/libpng license, which is an OSI-certified,
 *   BSD-like license that allows static linking with closed source software
 *
 *   Copyright (c) 2015-2024 Ramon Santamaria (@raysan5)
 *
 ********************************************************************************************/
#include "raylib.h"
#if defined(PLATFORM_WEB)
#include <emscripten/emscripten.h>
#endif
const int screenWidth = 500;
const int screenHeight = 400;
void UpdateDrawFrame(void);
bool loaded = false;
Camera camera;
Model model;
Model mtl;
Texture2D texture;
Vector3 position;
BoundingBox bounds;
bool selected = false;

int main(void)
{
  camera = (Camera){ 0 };
  camera.position = (Vector3){ 50.0f, 50.0f, 50.0f };
  camera.target = (Vector3){ 0.0f, 10.0f, 0.0f };
  camera.up = (Vector3){ 0.0f, 1.0f, 0.0f };
  camera.fovy = 45.0f;
  camera.projection = CAMERA_PERSPECTIVE;

  InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");
#if defined(PLATFORM_WEB)
  emscripten_set_main_loop(UpdateDrawFrame, 0, 1);
#else
  SetTargetFPS(60);
  while (!WindowShouldClose())
    {
      UpdateDrawFrame();
    }
#endif
  UnloadTexture(texture);
  UnloadModel(model);
  CloseWindow();
  return 0;
}

void UpdateDrawFrame(void)
{
  if (!loaded) {
    model = LoadModel("pawn.obj");
    texture = LoadTexture("concrete_texture.png");
    model.materials[0].maps[MATERIAL_MAP_DIFFUSE].texture = texture;
    position = (Vector3){ 0.0f, 0.0f, 0.0f };
    bounds = GetMeshBoundingBox(model.meshes[0]);
    loaded = true;
  }
  UpdateCamera(&camera, CAMERA_FIRST_PERSON);
  if (IsFileDropped()) {
      loaded = true;
      FilePathList droppedFiles = LoadDroppedFiles();

      if (droppedFiles.count == 1) // Only support one file dropped
	{
	  if (IsFileExtension(droppedFiles.paths[0], ".obj") ||
	      IsFileExtension(droppedFiles.paths[0], ".gltf") ||
	      IsFileExtension(droppedFiles.paths[0], ".glb") ||
	      IsFileExtension(droppedFiles.paths[0], ".vox") ||
	      IsFileExtension(droppedFiles.paths[0], ".iqm") ||
	      IsFileExtension(droppedFiles.paths[0], ".m3d"))       // Model file formats supported
	    {
	      UnloadModel(model);                         // Unload previous model
	      model = LoadModel(droppedFiles.paths[0]);   // Load new model
	      model.materials[0].maps[MATERIAL_MAP_DIFFUSE].texture = texture; // Set current map diffuse texture

	      bounds = GetMeshBoundingBox(model.meshes[0]);

	      // TODO: Move camera position from target enough distance to visualize model properly
	    }
	  else if (IsFileExtension(droppedFiles.paths[0], ".png"))  // Texture file formats supported
	    {
	      // Unload current model texture and load new one
	      UnloadTexture(texture);
	      texture = LoadTexture(droppedFiles.paths[0]);
	      model.materials[0].maps[MATERIAL_MAP_DIFFUSE].texture = texture;
	    }
	}

      UnloadDroppedFiles(droppedFiles);    // Unload filepaths from memory
    }

  // Select model on mouse click
  if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT))
    {
      // Check collision between ray and box
      if (GetRayCollisionBox(GetMouseRay(GetMousePosition(), camera), bounds).hit) selected = !selected;
      else selected = false;
    }
  BeginDrawing();
  ClearBackground(RAYWHITE);
  BeginMode3D(camera);
  DrawModel(model, position, 1.0f, WHITE);        // Draw 3d model with texture
  DrawGrid(20, 10.0f);         // Draw a grid
  if (selected) DrawBoundingBox(bounds, GREEN);   // Draw selection box
  EndMode3D();
  DrawText("Drag & drop model to load mesh/texture.", 10, GetScreenHeight() - 20, 10, DARKGRAY);
  if (selected) DrawText("MODEL SELECTED", GetScreenWidth() - 110, 10, 10, GREEN);
  DrawFPS(10, 10);
  EndDrawing();
}
