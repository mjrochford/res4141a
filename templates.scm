(define-module (res4141a templates)
  #:use-module (ice-9 unicode)
  #:use-module (ice-9 iconv)
  #:use-module (srfi srfi-1)
  #:use-module (res4141a chess)
  #:use-module (res4141a prelude)
  #:export (chess-char-table-html
             chess-table-html
             index-html))

(define bootstrap-jsdelivr
  '(; <!-- Latest compiled and minified CSS -->

    (link (@ (rel "stylesheet") (href "https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css") (integrity "sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu") (crossorigin "anonymous")))
                                        ; <!-- Optional theme -->
    (link (@ (rel "stylesheet") (href "https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap-theme.min.css") (integrity "sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ") (crossorigin "anonymous")))
                                        ; <!-- Latest compiled and minified JavaScript -->
                                        ; <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
    ))

(define (html-document title body)
  `(html (@ (lang "en"))
    (head
     ,bootstrap-jsdelivr
     (title ,title)
     (meta (@ (name "viewport") (content "width=device-width, initial-scale=1")))
     (meta (@ (charset "UTF-8")))
     (link (@ (href "style.css") (rel "stylesheet"))))
    (body ,body)))
;; TEMPLATES ----
(define chess-char-table-html
  (html-document "chess chars"
                 `((a (@ (href "/")) (h1 "<- HOME"))
                   (h2 (@ (class "border-bottom")) "CHAR TABLE")
                   (table (@ (class "table table-bordered"))
                          (tr (th "INDEX") (th "FORMAL") (th "CHAR"))
                          ,(for i in *chess-chars*
                                `(tr (td ,(list-index (curry eq? i) *chess-chars*))
                                     (td ,(char->formal-name i)) (td ,i))))
                   )))

(define (chess-table-html board)
  (html-document "chess"
                 `((a (@ (href "/")) (h1 "<- HOME"))
                   (style "tr { line-height: 1.2rem; }")
                   (style "th { width: 1.4rem; }")
                   (style "table { transform: scale(2.5); transform-origin: top left; }")
                   (style "table { border: 3px solid black; }")
                   (style "td { border: 1px solid black; }")
                   (style "td:hover { border: 1px solid #555; }")
                   (style "td { font-size: 1rem; }")
                   (style "th { font-size: 0.8rem; }")
                   (style "th, table { vertial-align:middle; text-align:center; } ")
                   (style "table { background-color: #bbb; } ")
                   ,(for i in (range 0 7)
                         (for j in (range 0 7)
                              `(style
                                   ,(format #f
                                            ".board-~a-~a { background-color: ~a; color: #67c } "
                                            i j (if (eq? 0 (mod (+ i j) 2)) "#aaa" "#151515")))
                              ))
                   (h2 "GAME")
                   (hr)
                   ,(board-render-html board)
                   )))

(define index-html
  (html-document "dawn"
                 '((h1 "~")
                   (a (@ (href "/godot/asdf.html")) (img (@ (src "banner.jpg") (width 256))))
                   (ul (h3 "links")
                       (a (@ (href "chartable")) (li "chartable"))
                       (a (@ (href "game")) (li "game"))
                       )
                   (a (@ (href "/raylib")) (button "psss"))
                   )))
