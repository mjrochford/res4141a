(define-module (res4141a server)
  #:use-module (res4141a prelude)
  #:use-module (res4141a templates)
  #:use-module (res4141a chess)
  #:use-module (ice-9 binary-ports)
  #:use-module (web uri)
  #:use-module (web request)
  #:use-module (web response)
  #:use-module (web server)
  #:export (main-handler))

;; GLOBALS ----
(define *game-board* (make-board 8))
(define *assets* '(("banner.jpg") ("style.css")))

;; MIDDLEWARE ----
(define (log-http request body)
  (format #t "[che][~a] ~a ~a ~a\n"
          'debug
          (request-method request)
          (uri->string
           (request-uri request)) (request-meta request)))

;; HANDLERS ----
(define (char-table-handler request body)
  (sxml->html-response chess-char-table-html))
(define (game-handler request body)
  (sxml->html-response (chess-table-html *game-board*)))
(define (index-handler request body)
  (sxml->html-response index-html))
(define (asset-handler path request body)
  (cond
   ((equal? path '("style.css"))
    (values
     '((content-type . (text/css)))
     "* { font-family: 'Iosevka', 'Iosevka Term', monospace; }
@font-face {
    font-family: 'Iosevka';
    src: url('https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/Iosevka/IosevkaNerdFont-Regular.ttf') format('truetype'); /* Chrome 4+, Firefox 3.5, Opera 10+, Safari 3—5 */
}  
html {
 box-sizing: border-box;
 margin:3rem;
}
body {
 filter: invert(100%);
 background-color: #333;
}"))
   ((equal? path '("banner.jpg"))
    (values
     '((content-type . (image/jpeg)))
     (call-with-input-file (if (eq? 0 (random 2))
                               "Gemini_Generated_Image_e2d7c6e2d7c6e2d7.jpg"
                               "Gemini_Generated_Image_kgnulekgnulekgnu.jpg")
       get-bytevector-all)
     ))
   (else (values '((content-type . (text/html))) "404"))
   )
  )

(define (main-handler request body)
  (begin
    (log-http request body)
    (let ((path (request-path-components request)))
      (cond
       ((equal? path '("chartable")) (char-table-handler request body))
       ((equal? path '("game")) (game-handler request body))
       ((member path *assets*) (asset-handler path request body))
       ((equal? path '()) (index-handler request body))
       (else (begin (format #t "not found ~a\n" path)
                    (values '((content-type . (text/plain))) "uhhh")))
       ))))

(run-server main-handler 'http '(#:port 8081 #:addr 0))
